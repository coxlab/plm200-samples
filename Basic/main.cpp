#include <cox.h>
#include <EVAM8M.hpp>

// IPv6 ipv6;
Timer timer1Sec, timer500mSec;
EVAM8M gps(SerialGPS);

static void eventSerialRx(SerialPort &) {
  while (Serial.available() > 0) {
    char c = Serial.read();

    Serial.write(c);
    if (c == '\r') {
      Serial.write('\n');
    }
  }
}

static void eventTimer1Sec(void *) {
  printf("[1sec]\n1111111111\n2222222222\n3333333333\n4444444444\n5555555555\n6666666666\n7777777777\n8888888888\n9999999999\n0000000000\n");
  Serial2.printf("[1sec]\n");
}

static void eventTimer500mSec(void *) {
  printf("[500msec]\n");
  //Serial2.printf("[500msec]\n");
}

static void eventGpsReadDone(
  uint8_t fixQuality,
  uint8_t hour,
  uint8_t minute,
  uint8_t sec,
  uint16_t subsec,
  int32_t latitude,
  int32_t longitude,
  int32_t altitude,
  uint8_t numSatellites
) {
  printf(
    "* [GPS] fixQ:%u, %02u:%02u:%02u.%02u, %c %ld.%ld, %c %ld.%ld, %ld.%ld, # of sat:%u\n",
    fixQuality,
    hour, minute, sec, subsec,
    latitude >= 0 ? 'N' : 'S', latitude / 100000, latitude % 100000,
    longitude >= 0 ? 'E' : 'W', longitude / 100000, longitude % 100000,
    altitude / 10, altitude % 10,
    numSatellites
  );
}

void setup() {
  Serial.begin(115200);
  printf("\n*** [PLM200] Basic Functions ***\n");
  Serial.listen();
  Serial.onReceive(eventSerialRx);

  Serial2.begin(115200);

  timer1Sec.onFired(eventTimer1Sec, NULL);
  timer1Sec.startPeriodic(1000);             //1000 msec = 1 sec

  timer500mSec.onFired(eventTimer500mSec, NULL);
  //timer500mSec.startPeriodicMicros(500000);  //500000 usec = 500 msec

  gps.begin();
  gps.onReadDone(eventGpsReadDone);
  gps.turnOn();
}
