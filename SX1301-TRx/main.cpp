#include <cox.h>

//![CenterFreq]
#define RADIO_A_FREQ  922100000
#define RADIO_B_FREQ  923100000
//![CenterFreq]

//![Rx]
static void eventRxDone(void *, GPIOInterruptInfo_t *) {
  while (SX1301.bufferIsEmpty() == false) {
    RadioPacket *rxFrame = new RadioPacket(125);

    if (!rxFrame) {
      printf("Out of memory to read frame\n");
      SX1301.flushBuffer();
      continue;
    }

    error_t err = SX1301.readFrame(rxFrame);
    if (err != ERROR_SUCCESS) {
      printf("* Rx read error:%d\n", err);
      delete rxFrame;
      continue;
    }

    printf("* Rx is done!: Frequency:%lu Hz, RSSI:%d dB, Timestamp:0x%08lX%08lX, CRC:%s, Modulation:",
           rxFrame->freq,
           rxFrame->power,
           (uint32_t) (rxFrame->timestamp >> 32) & 0xFFFFFFFFul,
           (uint32_t) (rxFrame->timestamp >> 0) & 0xFFFFFFFFul,
           (rxFrame->result == RadioPacket::SUCCESS) ? "OK" : "FAIL");

    if (rxFrame->modulation == Radio::MOD_LORA) {
      const char *strBW[] = { "?", "125kHz", "250kHz", "500kHz" };
      const char *strCR[] = { "?", "4/5", "4/6", "4/7", "4/8" };
      printf("LoRa, BW:%s, CR:%s, SF:%u, ",
              (rxFrame->meta.LoRa.bw < 4) ? strBW[rxFrame->meta.LoRa.bw] : "?",
              (rxFrame->meta.LoRa.cr < 5) ? strCR[rxFrame->meta.LoRa.cr] : "?",
              rxFrame->meta.LoRa.sf);

    } else if (rxFrame->modulation == Radio::MOD_FSK) {
      printf("FSK, ");
    } else {
      printf("?(%u), ", rxFrame->modulation);
    }

    printf("Length:%u (", rxFrame->len);
    for (uint16_t i = 0; i < rxFrame->len; i++)
      printf(" %02X", rxFrame->buf[i]);
    printf(" )\n");

    delete rxFrame;
  }
}
//![Rx]

//![Tx]
Timer timerSend;
RadioPacket *txFrame = NULL;
uint16_t sent = 0;

static void eventTxDone(void *ctx, bool success, GPIOInterruptInfo_t *) {
  printf("[%lu us] Tx %s!\n", micros(), (success) ? "SUCCESS" : "FAIL");
  delete txFrame;
  txFrame = NULL;
}

static void sendTask(void *args) {
  if (txFrame != NULL) {
    printf("Tx in progress...\n");
    return;
  }

  txFrame = new RadioPacket(17);
  if (!txFrame) {
    printf("Not enough memory\n");
    return;
  }

  //memcpy(txFrame->buf, "\x03\x28\x5F\x34\x12\xFF\xFF\x5F\x28\x01\x00\xAB\x89\x67\x45\x23\x01", 17);
  for (uint8_t n = 2; n < txFrame->len; n++) {
    txFrame->buf[n] = n;
  }

  txFrame->buf[0] = (sent >> 8);
  txFrame->buf[1] = (sent & 0xff);
  txFrame->buf[2] = sent;

#if 1
  txFrame->modulation = Radio::MOD_LORA;
  txFrame->meta.LoRa.sf = Radio::SF7;
  txFrame->meta.LoRa.cr = Radio::CR_4_5;
  txFrame->meta.LoRa.bw = Radio::BW_125kHz;
  txFrame->freq = 922100000;
#else
  txFrame->modulation = Radio::MOD_FSK;
  txFrame->meta.FSK.drBps = 50000;
  txFrame->meta.FSK.fDevHz = 25000;
  txFrame->freq = 921900000;
#endif
  txFrame->power = 0;

  error_t err = SX1301.transmit(txFrame);
  if (err == ERROR_SUCCESS) {
    printf("[%lu us] %u Tx started...\n", micros(), sent);
    sent++;
  } else {
    printf("[%lu us] Tx failed: %d\n", micros(), err);
    delete txFrame;
    txFrame = NULL;
  }
}
//![Tx]

//![setup]
void setup() {
  Serial.begin(115200);
  printf("\n*** [PLM200] SX1301 Tx/Rx ***\n");

  SX1301.setSyncword(0x12);

  SX1301.configureRf(0, true, true, RADIO_A_FREQ);
  SX1301.configureRf(1, true, false, RADIO_B_FREQ);

  SX1301.configureRxIf(0, 0, true, 921900000 - RADIO_A_FREQ); //Multi LoRa
  SX1301.configureRxIf(1, 0, true, 922100000 - RADIO_A_FREQ); //Multi LoRa
  SX1301.configureRxIf(2, 0, true, 922300000 - RADIO_A_FREQ); //Multi LoRa
  SX1301.configureRxIf(3, 0, true, 922500000 - RADIO_A_FREQ); //Multi LoRa
  SX1301.configureRxIf(4, 1, true, 922700000 - RADIO_B_FREQ); //Multi LoRa
  SX1301.configureRxIf(5, 1, true, 922900000 - RADIO_B_FREQ); //Multi LoRa
  SX1301.configureRxIf(6, 1, true, 923100000 - RADIO_B_FREQ); //Multi LoRa
  SX1301.configureRxIf(7, 1, true, 923300000 - RADIO_B_FREQ); //Multi LoRa
  SX1301.configureRxIf(8, 1, true, 923500000 - RADIO_B_FREQ); //LoRa standalone
  SX1301.configureRxIf(9, 0, true, 921700000 - RADIO_A_FREQ); //FSK standalone

  SX1301.onRxDone = eventRxDone;
  SX1301.onTxDone = eventTxDone;
  SX1301.begin();

  for (uint8_t i = 0; i < 10; i++) {
    printf("* Rx interface:#%u, Frequency:%lu Hz\n", i, SX1301.getChannel(i));
  }

  timerSend.onFired(sendTask, NULL);
  timerSend.startPeriodic(3000);
}
//![setup]
