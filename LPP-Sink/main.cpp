#include <cox.h>
#include <LPPMac.hpp>

#define RADIO_A_FREQ  922300000
#define RADIO_B_FREQ  923100000

uint16_t node_id = 1;
uint8_t node_ext_id[] = {0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0, 0};

LPPMac Lpp;

Timer timerSetup;

static void received(IEEE802_15_4Mac &radio, const IEEE802_15_4Frame *rxFrame) {
  IEEE802_15_4Address srcAddr = rxFrame->getSrcAddr();
  if (srcAddr.len == 2) {
    printf("RX : %x, ", srcAddr.id.s16);
  } else if (srcAddr.len == 8) {
    printf("RX : %02x-%02x-%02x-%02x-%02x-%02x-%02x-%02x, ",
           srcAddr.id.s64[0],
           srcAddr.id.s64[1],
           srcAddr.id.s64[2],
           srcAddr.id.s64[3],
           srcAddr.id.s64[4],
           srcAddr.id.s64[5],
           srcAddr.id.s64[6],
           srcAddr.id.s64[7]);
  }

  printf("Frequency:%lu Hz, RSSI:%d dB, Timestamp:0x%08lX%08lX, CRC:%s, Modulation:",
         rxFrame->freq,
         rxFrame->power,
         (uint32_t) (rxFrame->timestamp >> 32) & 0xFFFFFFFFul,
         (uint32_t) (rxFrame->timestamp >> 0) & 0xFFFFFFFFul,
         rxFrame->result == RadioPacket::SUCCESS ? "OK" : "FAIL");

  if (rxFrame->modulation == Radio::MOD_LORA) {
    const char *strBW[] = { "?", "125kHz", "250kHz", "500kHz" };
    const char *strCR[] = { "?", "4/5", "4/6", "4/7", "4/8" };
    printf("LoRa, BW:%s, CR:%s, SF:%u, ",
           (rxFrame->meta.LoRa.bw < 4) ? strBW[rxFrame->meta.LoRa.bw] : "?",
           (rxFrame->meta.LoRa.cr < 5) ? strCR[rxFrame->meta.LoRa.cr] : "?",
           rxFrame->meta.LoRa.sf);

  } else if (rxFrame->modulation == Radio::MOD_FSK) {
    printf("FSK, ");
  } else {
    printf("?(%u), ", rxFrame->modulation);
  }

  printf("Length:%u (", rxFrame->getPayloadLength());
  for (uint16_t i = 0; i < rxFrame->getPayloadLength(); i++)
    printf(" %02X", rxFrame->getPayloadAt(i));
  printf(" )\n");
}

static void receivedProbe(uint16_t panId,
                          const uint8_t *eui64,
                          uint16_t shortId,
                          int16_t rssi,
                          const uint8_t *payload,
                          uint8_t payloadLen,
                          uint32_t channel) {
  printf("* Probe received from PAN:0x%04X, "
        "Node EUI64:%02x-%02x-%02x-%02x-%02x-%02x-%02x-%02x, "
        "ID:%x, RSSI:%d",
        panId,
        eui64[0], eui64[1], eui64[2], eui64[3],
        eui64[4], eui64[5], eui64[6], eui64[7],
        shortId, rssi);

  if (payloadLen > 0) {
    uint8_t i;

    printf(", length:%u, ", payloadLen);
    for (i = 0; i < payloadLen; i++) {
      printf("%02X ", payload[i]);
    }
  }

  printf("\n");
}

void setup(void) {
  Serial.begin(115200);
  printf("\n*** [PLM200] LPP Sink ***\n");

  // SX1301.setSyncword(0x12);

  SX1301.setDataRate(Radio::SF7);
  SX1301.setCodingRate(Radio::CR_4_5);
  SX1301.setBandwidth(Radio::BW_125kHz);
  SX1301.setTxPower(0);
  SX1301.setChannel(922100000);

  SX1301.configureRf(0, true, true, RADIO_A_FREQ);
  SX1301.configureRf(1, true, false, RADIO_B_FREQ);

  SX1301.configureRxIf(0, 0, true, 921900000 - RADIO_A_FREQ); //Multi LoRa
  SX1301.configureRxIf(1, 0, true, 922100000 - RADIO_A_FREQ); //Multi LoRa
  SX1301.configureRxIf(2, 0, true, 922300000 - RADIO_A_FREQ); //Multi LoRa
  SX1301.configureRxIf(3, 0, true, 922500000 - RADIO_A_FREQ); //Multi LoRa
  SX1301.configureRxIf(4, 0, true, 922700000 - RADIO_A_FREQ); //Multi LoRa
  SX1301.configureRxIf(5, 1, true, 922900000 - RADIO_B_FREQ); //Multi LoRa
  SX1301.configureRxIf(6, 1, true, 923100000 - RADIO_B_FREQ); //Multi LoRa
  SX1301.configureRxIf(7, 1, true, 923300000 - RADIO_B_FREQ); //Multi LoRa
  SX1301.configureRxIf(8, 1, true, 923500000 - RADIO_B_FREQ,
    SX1301Chip::BW_125kHz, SX1301Chip::DR_LORA_SF7);          //LoRa standalone
  SX1301.configureRxIf(9, 0, false, 0);                       //FSK standalone (not used)

  SX1301.begin();

  node_ext_id[6] = highByte(node_id);
  node_ext_id[7] = lowByte(node_id);

  Lpp.begin(SX1301, 0x1234, 0xFFFF, node_ext_id);
  Lpp.setProbePeriod(3000);
  Lpp.setListenTimeout(3300);
  Lpp.setTxTimeout(632);
  Lpp.setRxTimeout(465);
  Lpp.setRxWaitTimeout(30);
  Lpp.setRadioAlwaysOn(true);

  Lpp.onReceive(received);
  Lpp.onReceiveProbe(receivedProbe);
}
